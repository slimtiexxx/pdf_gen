/**
 * Created by andras on 13/06/17.
 */
'use strict';

function htmlToPdf() {

    var content = $('#save-it')
        .find('p, h1, h2, h3, h4, h5, h6')
        .not('.ignore');


    var contents = [];

    // RGB TO HEX COLOR
    var hexDigits = ["0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"];

    function rgb2hex(rgb) {
        rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
    }

    function hex(x) {
        return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
    }
    // RGB TO HEX COLOR


    // GET CONTENTS DATA
    function getStyles(current, value) {
        var data = {};

        var text         =   current.text().replace(/\s+/g, " ").trim()
        var fontSize     =   parseInt(current.css('font-size'), 10);
        var lineHeight   =   1;
        var color        =   rgb2hex(current.css('color'));
        var marginTop    =   parseInt(current.css('margin-top'), 10);
        var marginBottom =   parseInt(current.css('margin-bottom'), 10);
        var marginLeft   =   parseInt(current.css('margin-left'), 10);
        var marginRight  =   parseInt(current.css('margin-right'), 10);
        var margin       =   [marginLeft, marginTop, marginRight, marginBottom];

        data.text        = text;
        data.fontSize    = fontSize;
        data.lineHeight  = lineHeight;
        data.color       = color;
        data.margin      = margin;

        return data;
    }
    // GET CONTENTS DATA

    $.each(content, function(index, value) {

        // paragraphs
        if ($(this).value = $('p')) {
            var p = getStyles($(this));
            contents.push(p);
        }

        // heading 1
        else if ($(this).value = $('h1')) {
            var h1 = getStyles($(this));
            contents.push(h1);
        }

        // heading 2
        else if ($(this).value = $('h2')) {
            var h2 = getStyles($(this));
            contents.push(h2);
        }

        // heading 3
        else if ($(this).value = $('h3')) {
            var h3 = getStyles($(this));
            contents.push(h3);
        }

        // heading 4
        else if ($(this).value = $('h4')) {
            var h4 = getStyles($(this));
            contents.push(h4);
        }

        // heading 5
        else if ($(this).value = $('h5')) {
            var h5 = getStyles($(this));
            contents.push(h5);
        }

        // heading 6
        else if ($(this).value = $('h6')) {
            var h6 = getStyles($(this));
            contents.push(h6);
        }
    });

    var docDefinition = {
        pageMargins: [ 30, 30, 30, 30 ],
        pageSize: 'A4',

        content: []
    };

    docDefinition.content.push(contents);

    pdfMake.createPdf(docDefinition).download();
};

$(document).ready(function () {
    $('#export').click(function () {
        htmlToPdf();
    });
});